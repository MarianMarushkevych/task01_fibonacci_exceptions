package Mar.Fibonacci;

public enum FibonacciMenu {
    GenerateByInterval,
    GenerateByCount,
    Exit
}
